<?php
$host = 'localhost';
$username = 'root';
$password = '';
$database = 'employee';

$conn = new mysqli($host, $username, $password, $database);

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Create (Insert) Operation
if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['create'])) {
    $first_name = $_POST['first_name'];
    $last_name = explode(",", $_POST['last_name']); // Convert to array
    $middle_name = $_POST['middle_name'];
    $birthday = $_POST['birthday'];
    $address = $_POST['address '];

    // Insert into 'employees' table using prepared statement
    $insertQuery = "INSERT INTO employee (first_name, last_name, middle_name, birthday, address) VALUES (?, ?, ?, ?, ?)";
    $stmt = $conn->prepare($insertQuery);
    $stmt->bind_param("siss", $first_name, $last_name, $middle_name, $birthday, $address);

    if ($stmt->execute()) {
        echo "New record created successfully";
    } else {
        echo "Error: " . $stmt->error;
    }
    $stmt->close();
}

// Read Operation
$selectQuery = "SELECT * FROM employee";
$result = $conn->query($selectQuery);

// Update Operation
if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['update'])) {
    $update_id = $_POST['update_id'];
    $new_address = $_POST['new_address'];

    $updateQuery = "UPDATE employee SET address = ? WHERE id = ?";
    $stmt = $conn->prepare($updateQuery);
    $stmt->bind_param("si", $new_address, $update_id);

    if ($stmt->execute()) {
        echo "Record updated successfully";
    } else {
        echo "Error updating record: " . $stmt->error;
    }
    $stmt->close();
}

// Delete Operation
if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['delete'])) {
    $delete_id = $_POST['delete_id'];

    $deleteQuery = "DELETE FROM employee WHERE id = ?";
    $stmt = $conn->prepare($deleteQuery);
    $stmt->bind_param("i", $delete_id);

    if ($stmt->execute()) {
        echo "Record deleted successfully";
    } else {
        echo "Error deleting record: " . $stmt->error;
    }
    $stmt->close();
}

$conn->close();
?>